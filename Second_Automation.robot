*** Settings ***
Library             String
Library             SeleniumLibrary

*** Variables ***
${browser}    chrome
${homepage}   automationpractice.com/index.php
${Schema}     http
${testURL}    ${Schema}://${homepage}

*** Keywords ***
Open homepage
    Open Browser    ${testURL}    ${browser}

*** Test cases ***
TC003 Hacer clic en contenedores
  Open homepage
  Set Global Variable    @{nombreContenedores}    //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
  FOR  ${nombreContenedor}   IN    @{nombreContenedores}
   Click Element   xpath=${nombreContenedor}
   Wait Until Element Is Visible   xpath=//*[@id="bigpic"]
   Click Element   xpath=//*[@id="header_logo"]/a/img
  END
  Close Browser
